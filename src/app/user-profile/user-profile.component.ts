import { Component, OnInit , AfterViewInit} from '@angular/core';
import {AppStateService} from '../services/app-state.service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

    public userProfile: any = {
        name:"",
        email:"",
        description:"",
        userProfileImage:"",
        userBackgroundImage:"",
    };

    public editMode: boolean = false;
    public isLoggedIn: boolean = false

    constructor( public appstate: AppStateService ) {}

    ngOnInit(): void {

    }

    ngAfterViewInit(){
        this.appstate.getAccountStoreData().subscribe( account => {

            const isValid = Object.entries(account).length > 0;

            if ( isValid ) {
                this.isLoggedIn = true;
                this.userProfile.email = account.email;
                this.userProfile.name = account.profile[0].name;
                this.userProfile.userProfileImage = account.profile[1].userProfileImage;
                this.userProfile.userBackgroundImage = account.profile[2].userBackgroundImage;
                this.userProfile.description = account.profile[3].description;

            } else {
                this.isLoggedIn = false;
            }

        });
    }

    public editProfile(form: NgForm): void {
        console.log("form:" , form)
    }

    public beginEditMode(){
        this.editMode = true;
        console.log("editing profile" , this.userProfile);
    }

    public cancel() {
        this.editMode = false;
    }
    public submit() {
        console.log("submitting profile");
    }

}

