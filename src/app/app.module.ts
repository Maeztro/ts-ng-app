import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MaterialModule} from './material/material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ItemCardComponent} from './item-card/item-card.component';
import {ItemTableComponent} from './item-table/item-table.component';
import {SidenavComponent} from './sidenav/sidenav.component';
import {ItemCardService} from './item-card/item-card.service';
import {HttpCrudService} from './services/http-crud.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login/login.component';
import {AuthentificationService} from './login/authentification.service';
import {RouterModule, Routes} from '@angular/router';
import { TagsNavComponent } from './tags-nav/tags-nav.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

const appRoutes: Routes = [
    {path: '', component: ItemCardComponent},
    {path: ':author', component: ItemCardComponent},
    {path: 'table', component: ItemTableComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
    declarations: [
        AppComponent,
        ItemCardComponent,
        ItemTableComponent,
        SidenavComponent,
        LoginComponent,
        TagsNavComponent,
        UserProfileComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        MaterialModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        AppRoutingModule
    ],
    providers: [
        ItemCardService,
        HttpCrudService,
        AuthentificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
