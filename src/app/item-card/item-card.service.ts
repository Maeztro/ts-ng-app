import { Injectable } from '@angular/core';
import {DataStore} from '../services/Datastore';
import {Observable} from 'rxjs';
import {HttpCrudService} from '../services/http-crud.service';
import {AuthentificationService} from '../login/authentification.service';

@Injectable({
    providedIn: 'root'
})
export class ItemCardService {

    protected defaultRoutes = {
        allItems: "/itemApi/getAll/items"
    };

    constructor(protected http_service: HttpCrudService, protected authentification_service: AuthentificationService) {}

    //DATASTORES

    protected item_store = new DataStore([]);

    getStoreData():Observable<Object[]>{
        return this.item_store.getState$();
    }

    //ITEMS

    public getDtoModel() {
        return this.http_service.get("/itemApi/getModel");
    }

    public getAllItems(route){
        console.log("GET ALL ITEMS ROUTE:" ,route);
        this.http_service.get(route)
            .subscribe(response => {
                this.item_store.setState(response);
                // console.log("FETCHED:" , response);
            },error =>{
                console.log('error',error);
            });
    };

    public getByAuthor(route){
        console.log("GET ALL ITEMS BY AUTHOR ROUTE:" ,route);
        this.http_service.get(route)
            .subscribe(response => {
                this.item_store.setState(response);
                // console.log("FETCHED:" , response);
            },error =>{
                console.log('error',error);
            });
    };

    public postItem(route, newItem){
        // console.log("POST ITEMS ROUTE:" ,route);
        const options = this.authentification_service.getAuthentification();

        this.http_service.post(route , newItem , options)
            .subscribe(response => {
                this.item_store.resetState();
                this. getAllItems(this.defaultRoutes.allItems);
                // console.log("FETCHED AFTER POST:" , response);
            },error =>{
                console.log('error',error);
            });
    };

    public editItem(route, oldItem, newItem){
        // console.log("EDIT ITEMS ROUTE:" , route);

        const options = this.authentification_service.getAuthentification();

        this.http_service.put(route , {oldItem:oldItem, newItem:newItem} , options)
            .subscribe(response => {
                this.item_store.resetState();
                this. getAllItems(this.defaultRoutes.allItems);
            },error =>{
                console.log('error',error);
            });
    };

    public deleteItem(route){
        console.log("DELETE ITEMS ROUTE:" , route);

        const options = this.authentification_service.getAuthentification();

        this.http_service.delete(route , options)
            .subscribe(response => {
                this.item_store.resetState();
                this. getAllItems(this.defaultRoutes.allItems);
                // console.log("FETCHED AFTER POST:" , response);
            },error =>{
                console.log('error',error);
            });
    };

}
