export class ItemData {
    public id?: string;
    public name: string;
    public title: string;
    public author: string;
    public subtitle: string;
    public url:string;
    public description: string;
    public index: number;
    public editMode: boolean;
}
