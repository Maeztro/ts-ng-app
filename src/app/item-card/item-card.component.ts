import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ItemCardService} from './item-card.service';
import {ActivatedRoute} from '@angular/router';
import {AppStateService} from '../services/app-state.service';

@Component({
    selector: 'app-item-card',
    templateUrl: './item-card.component.html',
    styleUrls: ['./item-card.component.scss']
})
export class ItemCardComponent implements OnInit {

    public itemList: any[] = [];
    public newItemData: any = {};
    public editItemData: any = {};
    public showNewItemForm: boolean = false;

    protected itemModel: any;

    constructor(
        private itemService: ItemCardService ,
        public appState : AppStateService,
        protected route: ActivatedRoute,
    ) {}

    ngOnInit() {
        this.itemService.getDtoModel()
            .subscribe( response => {
                // console.log("DTO MODEL :" , response);
                this.itemModel = response;
                this.getAllItems(this.route.snapshot.params['author']);
            } , error => console.log("ERROR:" , error));
    }

    public getAllItems(author?:string , category?: string){

        if( author ) {
            this.appState.setCurrentActivatedRoute( this.route.snapshot.params['author'] );
        }
        
        this.appState.refreshItems(author);

        this.itemService.getStoreData()
            .subscribe(response => {

                this.itemList = [];
                const tagList = ["all"];

                // the order of the posts is reversed because the latest posts must be shown first
                
                response.forEach( (item: any ) => {

                    if(category){
                        if(item.content.category === category){
                            this.itemList.push(item);
                        } else if ( category === "all" ){
                            this.itemList.push(item);
                        }
                    } else {
                        this.itemList.push(item);
                    }

                    tagList.push(item.content.category);

                });

                this.appState.refreshTags([...new Set(tagList)]);

                // console.log(this.itemList);

            });

    }

    public showNewItem() {
        this.newItemData = this.itemModel.content
        //reset this.newItemData with empty string values
        for ( let key of Object.entries(this.newItemData)) {
            this.newItemData[key[0]] = "";
        }
        this.showNewItemForm = true;
    }

    public hideNewItem() {
        this.showNewItemForm = false;
    }

    public submitNewCard(){

        const userProfile = {
            username: this.appState.getUserProfile().username,
            userId: this.appState.getUserProfile()._id,
            userProfileImage:this.appState.getUserProfile().profile[1].userProfileImage,
        }

        const newItem = {};

        //todo: might need to stick to a simpler structure or find a better way to loop through nested structures
        //todo: recursion? helper function that build objects based on a model?
        //todo this could also be used in accounts
        //todo: or mabie use an enum-like structure with functions attached to its values??

        for ( let [key , values ] of Object.entries(this.itemModel)){
            if(key === "_id") {
                newItem[key] = "";
            } else if ( key === "username"|| key === "userId" || key === "userProfileImage" ) {
                newItem[key] = userProfile[key];
            } else if ( key === "upVotes"|| key === "downVotes" ) {
                newItem[key] = 0;
            } else if ( key === "editMode") {
                newItem[key] = false;
            } else if ( key === "content") {
                newItem[key] = this.newItemData;
            }
        }

        this.itemService.postItem( "/itemApi/postOne/items", newItem);
        this.newItemData = {};
        this.hideNewItem();
    }

    public isEditAllowed ( userId: string , itemId: string ) {
        return userId === itemId;
    }

    public beginEditMode(index: number){

        if( this.isEditAllowed ( this.appState.getUserProfile()._id , this.itemList[index].userId)) {

            this.itemList[index].editMode = true;

            const userProfile = {
                username: this.appState.getUserProfile().username,
                userId: this.appState.getUserProfile()._id,
                userProfileImage:this.appState.getUserProfile().profile[1].userProfileImage,
            }

            for ( let [key , values ] of Object.entries(this.itemModel)){
                if(key === "_id") {
                    this.editItemData[key] = "";
                } else if ( key === "username"|| key === "userId" || key === "userProfileImage" ) {
                    this.editItemData[key] = userProfile[key];
                } else if ( key === "upVotes"|| key === "downVotes" ) {
                    this.editItemData[key] = 0;
                } else if ( key === "editMode") {
                    this.editItemData[key] = false;
                } else if ( key === "content") {
                    this.editItemData[key] = this.itemList[index][key];
                }
            }

        } else {
            console.error("THIS ITEM DOSENT BELONG TO THE CURRENT USER");
        }
    }

    public submitEditCard(oldItem , newItem){
        newItem.editMode = false;
        this.itemService.editItem( "/itemApi/editOne/items/", oldItem , newItem);
        this.editItemData = {}
    }

    public endEditMode(index: number){

        const editedItem = {};

        for(let [key] of Object.entries(this.editItemData)) {
            editedItem[key] = this.editItemData[key];
        }

        this.submitEditCard( this.itemList[index] , editedItem );
        this.itemList[index].editMode = false;
    }

    public deleteItem(index: number) {
        // console.log("IN ITEM CARD DELETING:" , this.itemList[index].id);
        this.itemService.deleteItem( "/itemApi/deleteOne/items/"+this.itemList[index]._id);
    }

    public filterTags(tagName: string) {
        // console.log(tagName);
        this.appState.setCurrentActivatedRoute( this.route.snapshot.params['author'] );
        this.getAllItems(this.route.snapshot.params['author'] , tagName);
    }

    @Output() onUpdateFilters = new EventEmitter<any>();

}
