import { TestBed } from '@angular/core/testing';

import { ItemCardService } from './item-card.service';

describe('ItemCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemCardService = TestBed.get(ItemCardService);
    expect(service).toBeTruthy();
  });
});

