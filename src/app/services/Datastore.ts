import {BehaviorSubject, Observable} from 'rxjs';

export class DataStore {
    protected currentState: BehaviorSubject<Object[]>;

    constructor(initialState: Object[]) {
        this.currentState = new BehaviorSubject(initialState);
    }

    getState$(): Observable<Object[]> {
        return this.currentState.asObservable();
    }

    getState(): Object[] {
        console.log('in DataStore', this.currentState.getValue());
        return this.currentState.getValue();
    }

    resetState():void {
        this.currentState.next([]);
    }

    setState(nextState: Object[]): void {
        this.currentState.next(nextState);
    }

    isEmpty():boolean{
        // console.log(this.currentState != null , this.currentState != undefined ,  this.currentState.getValue().length == 0);
        return this.currentState != null && this.currentState != undefined &&   this.currentState.getValue().length == 0;
    }
}
