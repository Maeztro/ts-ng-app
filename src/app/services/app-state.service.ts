import { Injectable } from '@angular/core';
import {ItemCardService} from '../item-card/item-card.service';
import {Observable, of} from 'rxjs';
import {DataStore} from './Datastore';

@Injectable({
    providedIn: 'root'
})
export class AppStateService {

    //DATASTORES

    protected tag_store = new DataStore([]);
    protected account_store = new DataStore([]);

    getTagStoreData():Observable<Object[]>{
        return this.tag_store.getState$();
    }
    
    getAccountStoreData(): Observable<any> {
        return this.account_store.getState$();
    }

    protected isLogged: boolean = false;
    protected userProfile: any;
    protected activatedRoute: any;

    constructor(private itemService: ItemCardService) {}

    public setLoggedStatus(status) {
        this.isLogged = status;
    }

    public setUserProfile(profile: any) {
        this.userProfile = profile;
        this.account_store.setState(profile); //todo this might just replace this.userProfile
    }

    public getLoggedStatus() {
        return this.isLogged;
    }

    public getUserProfile() {
        return this.userProfile;
    }

    public refreshItems(author?:string) {
        if(author) {
            this.itemService.getAllItems(`/itemApi/getByAuthor/items/${author}`);
        } else {
            this.itemService.getAllItems("/itemApi/getAll/items");
        }
    }

    public refreshTags(tags: any[]) {
        this.tag_store.resetState();
        this.tag_store.setState(tags);
    }

    public setCurrentActivatedRoute(route: any) {
        this.activatedRoute =  route;
    }

    public getCurrentActivatedRoute() {
        return this.activatedRoute;
    }


}
