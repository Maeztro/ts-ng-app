import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpCrudService {

    constructor(protected http: HttpClient) { }

    protected default_uri = 'http://localhost:3000';
    protected path_uri = 'http://localhost:3000';

    setRoute(new_route){
        this.path_uri = this.default_uri;
        if(new_route){
            this.path_uri = new_route;
        }
    }

    public get(route):Observable<any>{
        let url = this.path_uri + route;
        console.log("URL: " , url);
        return this.http.get(url)
    };

    public post(route,query,options?):Observable<any>{
        let url = this.path_uri + route;
        return this.http.post(url,query,options)
    };

    public put(route,query,options?):Observable<any>{
        let url = this.path_uri + route;
        return this.http.put(url,query,options)
    };

    public delete(route,options?):Observable<any>{
        let url = this.path_uri + route;
        return this.http.delete(url,options)
    };

}
