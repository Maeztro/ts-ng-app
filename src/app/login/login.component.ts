import { Component, OnInit } from '@angular/core';
import {AuthentificationService} from './authentification.service';
import {NgForm} from '@angular/forms';
import {AppStateService} from '../services/app-state.service';
import {Router , ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    public userProfile: any = [];
    public loggedIn: boolean = false;
    protected userModel: any = {};

    protected loginData = {
        username: "",
        password:""
    };

    constructor(
        protected authentification: AuthentificationService ,
        public appState: AppStateService ,
        protected router: Router,
        protected activatedRoute:ActivatedRoute
    ) {}

    ngOnInit() {
        this.authentification.getDtoModel()
            .subscribe( response => {
                // console.log("DTO MODEL VS CONFIG:" , response , this.userModel);
                for(let [key,value] of Object.entries(response)){
                    this.userModel[key] = response[key];
                }

                // console.log("USER MODEL: " , this.userModel)

            } , error => console.log("ERROR:" , error));
    }

    public login(form: NgForm) {
        // console.log("LOGIN FORM:" , form.value);
        this.authentification.login(form.value).subscribe( response => {

            if (response.status === 200) {
                this.startSession(response);
            } else {
                console.error(response.message);
            }

        });

    }

    public signin(form: NgForm) {
        console.log(form.value);

        const newAccount = {};

        console.log("config" ,this.userModel)

        for ( let [key , values ] of Object.entries(this.userModel)){
            if(key === "_id") {
                newAccount[key] = "";
            } else if (key === "profile") {
                newAccount[key] = [
                    { name: form.value.username, editable: true, visible:true },
                    { userProfileImage: "cat.jpg", editable: true, visible:true },
                    { userBackgroundImage: "mountain.jpg", editable: true, visible:true },
                    { description: "", editable: true, visible:true },
                    { active: true, editable: true, visible:false },
                ]
            } else {
                newAccount[key] = form.value[key]
            }
        }

        this.authentification.signIn(newAccount).subscribe( response => {
            console.log(response);
            this.authentification.login({username: form.value.username, password: form.value.password}).subscribe( response => {
                this.startSession(response);
                form.reset();
            })
        });
    }

    protected startSession(response: any){
        this.authentification.setToken(response.authorization);
        this.userProfile = response.user;
        this.appState.setUserProfile( response.user);
        this.loggedIn = true;
        this.appState.setLoggedStatus(true);
        this.appState.refreshItems();
        this.router.navigate([`${response.user.username}`],{relativeTo:this.activatedRoute});
    }

    public logout(){
        this.loggedIn = false;
        this.appState.setLoggedStatus(false);
        this.appState.setUserProfile({});
        this.authentification.logout();
        this.appState.refreshItems();
        this.router.navigate([""]);
    }

    public closeAccount(){

        const options = this.authentification.getAuthentification();

        this.authentification.cleanupPosts(this.userProfile , options ).subscribe( response => {
            console.log("RESPONSE: " , response);

            this.authentification.closeAccount(this.userProfile , options ).subscribe( response => {
                console.log("ACCOUNT CLOSED: " , response);
            });

        });

        this.appState.refreshItems();
        this.logout();

    }

}
