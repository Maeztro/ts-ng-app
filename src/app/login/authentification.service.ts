import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

    constructor(private http: HttpClient ) { }

    protected default_uri = 'http://localhost:3000';
    protected path_uri = 'http://localhost:3000';

    setRoute(new_route){
        this.path_uri = this.default_uri;
        if(new_route){
            this.path_uri = new_route;
        }
    }

    public getDtoModel() {
        const url = this.path_uri + "/accountsApi/getModel";
        return this.http.get(url)
    }


    public authenticate(query):Observable<any>{

        const url = this.path_uri + "/accountsApi/isAuth";

        const header = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("auth")
            }
        );

        const httpOptions = {
            headers: header
        };

        return this.http.post(url,query, httpOptions);

    };

    public login(query):Observable<any> {
        const url = this.path_uri + "/accountsApi/login";
        return this.http.post(url,query)
    };

    public signIn (query):Observable<any> {
        const url = this.path_uri + "/accountsApi/signup";
        // console.log("SIGN IN: " , query);

        return this.http.post(url,query)
    };

    public setToken( token ){
        localStorage.setItem("auth", token);
    }

    public logout(): void {
        localStorage.setItem("auth", null);
    };

    public getAuthentification() {
        const header = new HttpHeaders(
            {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem("auth")
            }
        );

        const httpOptions = {
            headers: header
        };

        return httpOptions;
    }

    public cleanupPosts(query, options){
        const url = this.path_uri + "/itemApi/deleteAllFromUser";
        return this.http.post(url,{userId: query._id } , options);
    }

    public closeAccount(query , options){
        const url = this.path_uri + "/accountsApi/closeAccount";
        return this.http.post(url,{userId: query._id } , options);
    }

}
