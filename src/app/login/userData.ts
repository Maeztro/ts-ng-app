export class userData {
    public _id?: string;
    public username: string;
    public email: string;
    public password: string;
    public items: [];
    public profile: {};
}
