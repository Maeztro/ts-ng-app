import {Component} from '@angular/core';
import {AppStateService} from './services/app-state.service';
import { Router, ActivatedRoute, Params, RoutesRecognized } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'ts-ng-app';
    protected userProfile;
    // protected router: Router;
    protected activeComponent: any;

    constructor( public appState: AppStateService ,
                 protected route: ActivatedRoute ,
                 protected router: Router){}

    ngOnInit() {
        this.userProfile = this.appState.getUserProfile();
    }

    public onActivate(activeComponent) {
        // console.log("COMPONENT IN ACTIVE ROUTE" ,activeComponent)
        this.activeComponent = activeComponent;
    }

    filterItemTags(tagsList: string) {
        console.log("THIS ROUTE PARAMS", this.route.snapshot.params['author'])
        this.activeComponent.getAllItems( this.route.snapshot.params['author'] , tagsList)
        // this.itemCards.getAllItems( null , tagName);
    }

}
