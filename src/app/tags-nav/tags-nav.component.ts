import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AppStateService} from '../services/app-state.service';

@Component({
    selector: 'app-tags-nav',
    templateUrl: './tags-nav.component.html',
    styleUrls: ['./tags-nav.component.scss']
})
export class TagsNavComponent implements OnInit {

    public tagsList = [];

    constructor( protected appState : AppStateService) {

    }

    ngOnInit() {
        this.appState.getTagStoreData().subscribe( tagsList => {
            this.tagsList = tagsList;
        })
    }

    public filterTags(tagName: string) {
        console.log(tagName);
        this.onUpdateFilters.emit(tagName);
    }

    @Output() onUpdateFilters = new EventEmitter<any>();

}
